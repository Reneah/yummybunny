﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace YummyBunny
{
    public class StartSceneLoader : MonoBehaviour
    {
        //gives field to link Scene you want to load: put Name of Scene you want to load to sceneName field
        [SerializeField]
        private string sceneName;

        //here you can insert the seconds for the delay before the StartScreen-Scene loads the Menu-Scene
        [SerializeField]
        private float delayBeforeLoading = 3;

        // Update is called once per frame
        void Update()
        {
            //decreases the delayBeforeoading and Loads the scene if the delayBeforeLoading is 0 or below or any key is pressed
            delayBeforeLoading -= Time.deltaTime;
            if (delayBeforeLoading <= 0 || Input.anyKeyDown)
            {
                LoadScene();
            }
        }

        //loads Scene
        public void LoadScene()
        {
            //gets Funktion (LoadScene()) out of Unity-Class SceneManager 
            SceneManager.LoadScene(sceneName);
        }
    }
}

