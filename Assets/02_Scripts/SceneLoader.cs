﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace YummyBunny
{
    public class SceneLoader : MonoBehaviour
    {
        //gets name of scene you want to load: put name of scene you want to load to sceneName field
        public string sceneName;
    
        //loads Scene
        public void LoadScene()    
        {
            //gets function (LoadScene()) from Unity-Class SceneManager
            SceneManager.LoadScene(sceneName);
        }
    }

}
