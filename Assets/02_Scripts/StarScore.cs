﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace YummyBunny
{
    public class StarScore : MonoBehaviour
    {
        //references the second star gameObject in canvas
        [SerializeField]
        private GameObject starTwo;

        //references the third star gameObject in canvas
        [SerializeField]
        private GameObject starThree;

        //references the BunnyScorer component in the BunnyScore gameObject
        [SerializeField]
        private BunnyScorer myBunnyScorer;

        //references the LifeCounter component in the LifeCounter gameObject
        [SerializeField]
        private LifeCounter myLifeCounter;

        //declares the counting for the stars with integer
        [SerializeField]
        private int starCounter;

        //declares the variable if the level is won (substitute for bool because PlayerPrefs can't save booleans)
        private int levelWon;

        //declares an event that specifies which SetScore function should be executed, depending on which Level StarScore should be obtained
        [SerializeField]
        private UnityEvent onLevelComplete;


        // Start is called before the first frame update
        void Start()
        {
            //gets the BunnyScorer component of the BunnyScore gameObject
            myBunnyScorer = GameObject.Find("BunnyScore").GetComponent<BunnyScorer>();
            //gets the LifeCounter component of the LifeCounter gameObject
            myLifeCounter = GameObject.Find("LifeCounter").GetComponent<LifeCounter>();
            //executes the count up function when the WinScreen is ativated
            CountUp();
        }

        public void CountUp()
        {
            //increases the star counter by one when the level is won, meaning the WinScreen gameObject is active
            starCounter = 1;
            //checks if the max. count of bunnies is reached
            if (myBunnyScorer.bunnyScore == myBunnyScorer.MaxBunnies)
            {
                //increments the starCounter by one
                starCounter += 1;
                Debug.Log("bunny count up");
            }
            //checks if the player has max health
            if (myLifeCounter.LifeValue >= 3)
            {
                //increments the starCounter by one
                starCounter += 1;
                Debug.Log("life count up");
            }
            //executes the SetScore() function
            SetScore();
            Debug.Log("Set Star Score to:" + starCounter);
        }

        //specifies how many stars are shown
        public void SetScore()
        {
            //shows two stars active
            if (starCounter == 2)
            {
                starTwo.GetComponent<Image>().color = Color.white;
            }
            //shows 3 stars active
            if (starCounter >= 3)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                starThree.GetComponent<Image>().color = Color.white;
            }
            //executes the onLevelComplete() event when the Score is set
            onLevelComplete.Invoke();
        }

        //Sets the score of level one
        public void SetScoreLevelOne()
        {
            //checks if the currentStarCounter is higher than the saved PlayerPref
            if(starCounter >= PlayerPrefs.GetInt("SCORE1"))
            {
                //sets levelWon value to 1 (substitute for bool = true)
                levelWon = 1;
                //saves the scores
                PlayerPrefs.SetInt("LEVELONEWON", levelWon);
                PlayerPrefs.SetInt("SCORE1", starCounter);
                PlayerPrefs.Save();
                Debug.Log("Star score Level1 saved: " + starCounter);
            }
        }

        //Sets the score of level two
        public void SetScoreLevelTwo()
        {
            //checks if the currentStarCounter is higher than the saved PlayerPref
            if (starCounter >= PlayerPrefs.GetInt("SCORE2"))
            {
                //sets levelWon value to 1 (substitute for bool = true)
                levelWon = 1;
                //saves the scores
                PlayerPrefs.SetInt("LEVELTWOWON", levelWon);
                PlayerPrefs.SetInt("SCORE2", starCounter);
                PlayerPrefs.Save();
                Debug.Log("Star score Level2 saved: " + starCounter);
            }
        }

        //Sets the score of level three
        public void SetScoreLevelThree()
        {
            //checks if the currentStarCounter is higher than the saved PlayerPref
            if (starCounter >= PlayerPrefs.GetInt("SCORE3"))
            {
                //sets levelWon value to 1 (substitute for bool = true)
                levelWon = 1;
                //saves the scores
                PlayerPrefs.SetInt("LEVELTHREEWON", levelWon);
                PlayerPrefs.SetInt("SCORE3", starCounter);
                PlayerPrefs.Save();
                Debug.Log("Star score Level3 saved: " + starCounter);
            }
        }
    }

}
