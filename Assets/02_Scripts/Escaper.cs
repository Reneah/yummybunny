﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace YummyBunny
{
    public class Escaper : MonoBehaviour
    {
        void Update()
        {
            //deactivates GameObjects this script is attached to when Escape is pressed
            if(Input.GetKey(KeyCode.Escape))
            {
                gameObject.SetActive(false);
            }
        }
    }
}
