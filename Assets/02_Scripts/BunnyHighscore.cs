﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class BunnyHighscore : MonoBehaviour
{
    //declares an event that specifies which GetBunnyScore function should be executed, depending on which level scene this script is used
    [SerializeField]
    private UnityEvent getBunnyHighscore;

    //references the text of the bunnyScore
    [SerializeField]
    private TextMeshProUGUI bunnyScoreText;

    //declares the max number of bunnies in Level One with integer
    [SerializeField]
    private int maxBunniesOne;

    //declares the max number of bunnies in Level Two with integer
    [SerializeField]
    private int maxBunniesTwo;

    //declares the max number of bunnies in Level Three with integer
    [SerializeField]
    private int maxBunniesThree;

    //Start is called before the first frame update
    void Update()
    {
        //Updates the event so the text is always updated e.g. when the Reset Highscore Button is pressed
        getBunnyHighscore.Invoke();
    }

    //gets the Highscore of level one
    public void GetBunnyScoreLevelOne()
    {
        //sets the bunnyScore text to the saved "BUNNYONE" key
        bunnyScoreText.text = "Yummy Bunnies: " + PlayerPrefs.GetInt("BUNNYONE").ToString() + "/" + maxBunniesOne;
    }

    //gets the Highscore of level two
    public void GetBunnyScoreLevelTwo()
    {
        //sets the bunnyScore text to the saved "BUNNYTWO" key
        bunnyScoreText.text = "Yummy Bunnies: " + PlayerPrefs.GetInt("BUNNYTWO").ToString() + "/" + maxBunniesTwo;
    }

    //gets the Highscore of level three
    public void GetBunnyScoreLevelThree()
    {
        //sets the bunnyScore text to the saved "BUNNYTHREE" key
        bunnyScoreText.text = "Yummy Bunnies: " + PlayerPrefs.GetInt("BUNNYTHREE").ToString() + "/" + maxBunniesThree;
    }
}
