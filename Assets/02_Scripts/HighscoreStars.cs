﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace YummyBunny
{
    public class HighscoreStars : MonoBehaviour
    {
        //references the first star
        [SerializeField]
        private GameObject starOne;

        //references the second star
        [SerializeField]
        private GameObject starTwo;

        //references the third star
        [SerializeField]
        private GameObject starThree;

        //declares an event that specifies which GetScore function should be executed, depending on which Level StarScore should be obtained
        [SerializeField]
        private UnityEvent getHighscore;

        // Start is called before the first frame update
        void Start()
        {
            //executes the getHighscore event on Start
            getHighscore.Invoke();
        }

        private void Update()
        {
            //checks if the Level are won and sets all stars to black if the WON-PLayerPrefs are 0
            if (PlayerPrefs.GetInt("LEVELONEWON") == 0 && PlayerPrefs.GetInt("LEVELTWOWON") == 0 && PlayerPrefs.GetInt("LEVELTHREEWON") == 0)
            {
                starOne.GetComponent<Image>().color = Color.black;
                starTwo.GetComponent<Image>().color = Color.black;
                starThree.GetComponent<Image>().color = Color.black;
                Debug.Log("DELETE ALL STARS");
            }
        }

        //gets the Highscore of level one
        public void GetScoreLevelOne()
        {
            //checks if the saved "LEVELONEWON" key is 1 and sets the color of the referenced first star to white, so the star is visible
            if (PlayerPrefs.GetInt("LEVELONEWON") == 1)
            {
                starOne.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star One active");
            }
            //checks if the saved "SCORE1" key is 2 and sets the color of the refrenced second star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE1") == 2)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two active");
            }
            //checks if the saved "SCORE1" key is 3 and sets the color of the refrenced third star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE1") >= 3 )
            {
                starTwo.GetComponent<Image>().color = Color.white;
                starThree.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two and Three active");
            }
        }

        //gets the Highscore of level two
        public void GetScoreLevelTwo()
        {
            //checks if the saved "LEVELTWOWON" key is 1 and sets the color of the first star to white, so the star is visible
            if (PlayerPrefs.GetInt("LEVELTWOWON") == 1)
            {
                starOne.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star One active");
            }
            //checks if the saved "SCORE2" key is 2 and sets the color of the refrenced second star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE2") == 2)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two active");
            }
            //checks if the saved "SCORE2" key is 3 and sets the color of the refrenced third star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE2") >= 3)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                starThree.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two and Three active");
            }
        }

        //gets the Highscore of level three
        public void GetScoreLevelThree()
        {
            //checks if the saved "LEVELTHREEWON" key is 1 and sets the color of the first star to white, so the star is visible
            if (PlayerPrefs.GetInt("LEVELTHREEWON") == 1)
            {
                starOne.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star One active");
            }
            //checks if the saved "SCORE3" key is 2 and sets the color of the refrenced second star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE3") == 2)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two active");
            }
            //checks if the saved "SCORE3" key is 3 and sets the color of the refrenced third star to white, so the star is visible
            if (PlayerPrefs.GetInt("SCORE3") >= 3)
            {
                starTwo.GetComponent<Image>().color = Color.white;
                starThree.GetComponent<Image>().color = Color.white;
                Debug.Log("Set Highscore Star Two and Three active");
            }
        }
    }
}

