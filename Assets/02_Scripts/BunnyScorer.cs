﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

namespace YummyBunny
{
    public class BunnyScorer : MonoBehaviour
    {
        //declares the Bunny Score with integer
        public int bunnyScore;

        //references the text of the score text ingame
        private TextMeshProUGUI scoreText;

        //references the text of the score text in the winnign screen
        [SerializeField]
        private TextMeshProUGUI winScoreText;

        //declares the max number of bunnies ingame with integer
        [SerializeField]
        private int maxBunnies;

        //declares an event that specifies which SetBunnyScore function should be executed, depending on which level scene this script is used
        [SerializeField]
        private UnityEvent setBunnyScore;

        //gets and sets the value of the maxBunnies variable so the public MaxBunnies property can be used in other scripts
        public int MaxBunnies
        {
            get { return maxBunnies; }
            set { maxBunnies = value; }
        }

        // Start is called before the first frame update
        void Start()
        {
            //sets value for counted Bunnies to 0 at the start
            bunnyScore = 0;
            //gives the variable scoreText a link to the Text Component this script is attached to
            scoreText = GetComponent<TextMeshProUGUI>();
        }

        // Update is called once per frame
        void Update()
        {
            //Updates the set setBunnyScore Event so the score is always saved
            setBunnyScore.Invoke();
            //sets the UI Text ingame to the current bunnyScore
            scoreText.text = bunnyScore + "/" + maxBunnies;
            //sets the win screen score text to the same as the variable scoreText
            winScoreText.text = scoreText.text;
        }

        //sets the Bunny Score in Level One
        public void SetBunnyScoreLevelOne()
        {
            //checks if the current bunny score is higher than the saved value in PlayerPrefs, because the Highscore should not get lower
            if (bunnyScore > PlayerPrefs.GetInt("BUNNYONE"))
            {
                //sets the int variable bunnyScore to the "BUNNYONE" key
                PlayerPrefs.SetInt("BUNNYONE", bunnyScore);
                //saves the "BUNNYONE" key
                PlayerPrefs.Save();
            }
        }

        //sets the Bunny Score in Level Two
        public void SetBunnyScoreLevelTwo()
        {
            //checks if the current bunnyScore is higher than the saved value in PlayerPrefs, because the Highscore should not get lower
            if (bunnyScore > PlayerPrefs.GetInt("BUNNYTWO"))
            {
                //sets the int variable bunnyScore to the "BUNNYTWO" key
                PlayerPrefs.SetInt("BUNNYTWO", bunnyScore);
                //saves the "BUNNYTWO" key
                PlayerPrefs.Save();
            }
        }

        //sets the Bunny Score in Level Three
        public void SetBunnyScoreLevelThree()
        {
            //checks if the current bunnyScore is higher than the saved value in PlayerPrefs, because the Highscore should not get lower
            if (bunnyScore > PlayerPrefs.GetInt("BUNNYTHREE"))
            {
                //sets the int variable bunnyScore to the "BUNNYTHREE" key
                PlayerPrefs.SetInt("BUNNYTHREE", bunnyScore);
                //saves the "BUNNYONE" key
                PlayerPrefs.Save();
            }
        }
    }
}
