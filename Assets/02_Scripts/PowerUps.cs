﻿using UnityEngine;

namespace YummyBunny
{
    public class PowerUps : MonoBehaviour
    {
        //declares which state the PowerUp gameObject has
        [SerializeField]
        private PowerUp powerUpState;

        //references the Player component of the Player gameObject
        [SerializeField]
        private Player myPlayer;

        //references the LifeCounter component of the LifeCounter gamebObject
        [SerializeField]
        private LifeCounter myLifeCounter;

        //references the pillow gameObject in canvas
        [SerializeField]
        private GameObject pillow;

        //refrences the playerPillow gameObject that appears on the player
        [SerializeField]
        private GameObject playerPillow;

        //refrences the playerRollerSkates gameObject that appears on the player
        [SerializeField]
        private GameObject playerRollerSkates;

        //refrences the Choppers gameObject that appears on the player
        [SerializeField]
        private GameObject playerChoppers;

        //declares if the PowerUpArmor is collected with bool
        [SerializeField]
        private bool collectedArmor;

        //declares if the PowerUpBitePower is collected with bool
        [SerializeField]
        private bool collectedBitePower;

        //gets and sets the value of the powerUpState variable so the public PowerUpState property can be used in other scripts
        public PowerUp PowerUpState
        {
            get { return powerUpState; }
            set { powerUpState = value; }
        }

        //gets and sets the value of the collectedArmor variable so the public CollectedArmor property can be used in other scripts
        public bool CollectedArmor
        {
            get { return collectedArmor; }
            set { collectedArmor = value; }
        }

        //gets and sets the value of the collectedBitePower variable so the public CollectedBitePower property can be used in other scripts
        public bool CollectedBitePower
        {
            get { return collectedBitePower; }
            set { collectedBitePower = value; }
        }
        //sets the states for the enum
        public enum PowerUp
        {
            None,
            SpeedBuff,
            BitePower,
            Armor
        }

        public void ChoosePowerUp()
        {
            //sets the function each state has
            switch (powerUpState)
            {
                case PowerUp.None:
                    break;

                //sets playerSpeed to 10 and activates the RollerSkates gameObject of the Player
                case PowerUp.SpeedBuff:
                    myPlayer.MoveSpeed = 10;
                    playerRollerSkates.SetActive(true);
                    break;
                
                //sets the collectedBitePower bool to true and activates the Choppers gameObject of the player
                case PowerUp.BitePower:
                    collectedBitePower = true;
                    playerChoppers.SetActive(true);
                    break;

                //sets the collectedArmor bool to true and makes the PlayerPillow gameObject visible
                case PowerUp.Armor:
                    pillow.SetActive(true);
                    playerPillow.GetComponent<SpriteRenderer>().enabled = true;
                    collectedArmor = true;
                    break;
            }
        }
    }
}

