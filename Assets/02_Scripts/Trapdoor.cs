﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class Trapdoor : MonoBehaviour
    {
        //references the Animator component of the TrapdoorAnchor gameObject
        [SerializeField]
        private Animator trapdoorAnimator;

        //initializes this string variable to "Player"
        private string playerTag = "Player";

        private void Start()
        {
            //gets the Animator of the child gameObject TrapdoorAnchor
            trapdoorAnimator = gameObject.GetComponentInChildren<Animator>();
        }

        //sets the "Open" bool in the parameter of the Animatior to true and "Close" to false to play the TrapdoorOpen Animation
        public void OpenTrapdoor()
        {
            trapdoorAnimator.SetBool("Open", true);
            trapdoorAnimator.SetBool("Close", false);
            Debug.Log("Open trap");
        }

        //sets the "Close" bool in the parameter of the Animatior to true and "Open" to false to play the TrapdoorClosed Animation
        public void CloseTrapdoor()
        {
            trapdoorAnimator.SetBool("Close", true);
            trapdoorAnimator.SetBool("Open", false);
            Debug.Log("Close trap");
        }

        //specifies what happens if something enters the collider of the trapdoor
        public void OnTriggerEnter2D(Collider2D playerCollider)
        {
            //checks if the collider that collides is tagges with "Player"
            if (playerCollider.gameObject.tag == playerTag)
            {
                //executes the OpenTrapdoor() function
                OpenTrapdoor();
            }
        }

        //specifies what happens if something exits the collider of the trapdoor
        public void OnTriggerExit2D(Collider2D playerCollider)
        {
            //checks if the collider that collides is tagges with "Player"
            if (playerCollider.gameObject.tag == playerTag)
            {
                //executes the CloseTrapdoor() function
                CloseTrapdoor();
            }
        }
    }
}
