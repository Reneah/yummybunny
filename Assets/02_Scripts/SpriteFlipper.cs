﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class SpriteFlipper : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            //gets the local scale of the FoxSpriteParent and checks if the player is moved on the horizontal axis and sets its local scale respectively to make the sprite flip
            Vector3 characterScale = transform.localScale;
            if (Input.GetAxis("Horizontal") < 0)
            {
                characterScale.x = -1;
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                characterScale.x = 1;
            }
            transform.localScale = characterScale;
        }
    }
}

