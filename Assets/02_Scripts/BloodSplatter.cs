﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class BloodSplatter : MonoBehaviour
    {
        //references the BloodSplatter Particle System that should be played
        [SerializeField]
        private ParticleSystem myParticleSystem;
        //references the Audio Source of the gameObject
        [SerializeField]
        private AudioSource bloodSplatterAudio;

        public void StartBloodSplatter()
        {
            //plays the BloodPlatter Particle System
            myParticleSystem.Play();
            //plays the Audio Source
            bloodSplatterAudio.Play();
        }  
    }
}
