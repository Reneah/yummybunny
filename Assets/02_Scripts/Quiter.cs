﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class Quiter : MonoBehaviour
    {
        //Quits Game (should be linked with Quit-Button)
        public void QuitGame()
        {
            //Gets [Quit()] function out of UnityEngine Class [Application]
            Application.Quit();
            //shows in Unity if function works by clicking Button
            Debug.Log("Game Quit");
        }
    }
}

