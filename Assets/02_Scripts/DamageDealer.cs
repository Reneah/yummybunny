﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class DamageDealer : MonoBehaviour
    {
        //references the LifeCounter Script of the LifeCounter gameObject
        [SerializeField]
        private LifeCounter myLifeCounter;

        //initializes string variable to "DamageDealer", meaning all gameObjects that deal damage to the player
        private string damageDealerTag = "DamageDealer";

        //initializes string variable to "Death", meaning all gameObjects that mean instant death to the player
        private string deathTag = "Death";

        //references the rigidbody of the player
        private Rigidbody2D myRigidbody2D;

        //references the PowerUps script of the PowerUpArmor gamObject
        [SerializeField]
        private PowerUps armorPowerUp;

        //references the gameObject Pillow in the Canvas
        [SerializeField]
        private GameObject pillow;

        //refrences the gameObject playerPillow that appears on the player
        [SerializeField]
        private GameObject playerPillow;

        //refrences the fox hit Audio Source that is attached to the player
        [SerializeField]
        private AudioSource damageSound;

        //refrences the FoxSprite gameObject
        [SerializeField]
        private GameObject foxSprite;

        //declares a variable that checks if the player is dead with bool
        private bool playerIsDead;

        //initializes the damage delay with float
        [SerializeField]
        private float damageDelay = 1f;

        //initializes the last damage with float
        [SerializeField]
        private float lastDamage = -10.0f;

        //declares the time that needs to pass until the player gets damage again with float
        [SerializeField]
        private float armorTime;
        //declares the timer that counts down until the player gets damage again with float
        [SerializeField]
        private float armorTimer;
        

        // Start is called before the first frame update
        void Start()
        {
            //gets the Rigidbody2D Component of the gameObject this script is attached to (player)
            myRigidbody2D = GetComponent<Rigidbody2D>();
            //sets the armorTimer variable to the value of the armorTime variable
            armorTimer = armorTime;
        }

        //specifies what happens when the player is colliding with collider of a gameObject that deals damage
        void OnTriggerStay2D(Collider2D damageCollider)
        {
            //checks if the collider the player collides with is tagged with "DamageDealer"
            if (damageCollider.gameObject.tag == damageDealerTag)
            {
                //checks if the PowerUpArmor gameObject's PowerUp State is set to Armor and if its CollectedArmor bool is true, meaning that the player collected the armor Power Up
                if (armorPowerUp.PowerUpState == PowerUps.PowerUp.Armor && armorPowerUp.CollectedArmor)
                {
                    //checks if t armorTimer is equal to the armorTime so the sound only plays once on collision
                    if(armorTimer == armorTime)
                    {
                        //plays the Audio Source that is attached to the PlayerPillow gameObject
                        playerPillow.GetComponent<AudioSource>().Play();
                    }
                    
                    //disables the PlayerPillow's Sprite Renderer so it isn't visible anymore
                    playerPillow.GetComponent<SpriteRenderer>().enabled = false;
                    //deactivates the Pillow gameObject
                    pillow.SetActive(false);

                    //checks if the armorTimer isn't null or below
                    if (armorTimer >= 0f)
                    {
                        //decreases the timer with deltaTime
                        armorTimer -= Time.deltaTime;
                        //terminates the execution of this method
                        return;
                    }
                    //executed if the statement above isn't true, meaning the timer has run out
                    else
                    {
                        //sets the armorTimer variable back to the armorTime value
                        armorTimer = armorTime;
                        //sets the PowerUp State to None and the CollectedArmor bool to flase so the second if-statement isn't true anymore and doesn't get called again and the player gets damage again
                        armorPowerUp.PowerUpState = PowerUps.PowerUp.None;
                        armorPowerUp.CollectedArmor = false;
                    }
                }

                //checks if the player is still alive
                if(!playerIsDead)
                {
                    if (Time.time > damageDelay + lastDamage)
                    {
                        //decreases the life Value variable of the referenced LifeCounter by 1
                        myLifeCounter.LifeValue -= 1;
                        //play the referenced damage sound
                        damageSound.Play();

                        //checks if an Animator component exists on the gameObject the player collides with
                        if (damageCollider.GetComponent<Animator>())
                        {
                            //sets the Trigger of the Animator to "Trap", so the trap animation is called
                            damageCollider.GetComponent<Animator>().SetTrigger("Trap");
                        }
                        //gets the BloodSplatter component that is attached to this gameObject and starts its method StartBloodSplatter()
                        GetComponent<BloodSplatter>().StartBloodSplatter();
                        //gets the SpriteRenderer component of the FoxSprite and changes its color to red
                        foxSprite.GetComponent<SpriteRenderer>().color = Color.red;
                        //starts the enumerator DamageColorDuration
                        StartCoroutine(DamageColorDuration());
                        //sets the lastDamage variable to the value of Time.time
                        lastDamage = Time.time;

                        //checks if the LifeValue variable is 0 or below and sets the playerIsDead bool to true so the player doesn't gets damage after death
                        if(myLifeCounter.LifeValue <= 0)
                        {
                            playerIsDead = true;
                        }
                    }
                }
                Debug.Log("Collide with Trap");
            }

            //checks if the collider the player collides with is tagged with "Death"
            if (damageCollider.gameObject.tag == deathTag)
            {
                //sets the lifeValue variable to 0
                myLifeCounter.LifeValue = 0;
                //disables the player gameObject so it doesn't fall forever
                gameObject.SetActive(false);
            }

            IEnumerator DamageColorDuration()
            {
                //waits for 0.2 seconds until the color of the SpriteRenderer component of the referenced FoxSprite is set back to white
                yield return new WaitForSeconds(0.2f);
                foxSprite.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }

        //checks if the player is exiting the collider the player collided with
        private void OnTriggerExit2D(Collider2D damageCollider)
        {
            //checks if the collider the player collides with is tagged with "DamageDealer" and if the PowerUpArmor gameObject's PowerUp State is set to Armor and if its CollectedArmor bool is true, meaning that the player collected the armor Power Up
            if (damageCollider.gameObject.tag == damageDealerTag && armorPowerUp.PowerUpState == PowerUps.PowerUp.Armor && armorPowerUp.CollectedArmor)
            {
                //sets the armor Timer to 0 so when the player exits the collider the armorTimer isn't just paused but reset
                armorTimer = 0;
            }
        }
    }
}
