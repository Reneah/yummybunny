﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace YummyBunny
{
    public class PauseGame : MonoBehaviour
    {
        //references the FoxSprite gameObject
        [SerializeField]
        private GameObject foxSprite;

        //references the PauseMenu gameObject
        [SerializeField]
        private GameObject pauseMenu;

        //references the Player gameObject
        [SerializeField]
        private GameObject player;

        //references the WinScreen gameObject
        [SerializeField]
        private GameObject winScreen;

        //references the GameOverScreen gameObject
        [SerializeField]
        private GameObject gameOverScreen;

        void Update()
        {
            //checks if the GameOver and WinScreen are both inactive in hierarchy to prevent overlapping of the screens
            if(gameOverScreen.activeInHierarchy == false && winScreen.activeInHierarchy == false)
            {
                //checks for Esc input to open the Pause Menu
                if (Input.GetKey(KeyCode.Escape))
                {
                    //makes the PauseMenu visible
                    pauseMenu.SetActive(true);
                    //stops the Player from moving
                    player.GetComponent<Player>().enabled = false;
                    foxSprite.GetComponent<SpriteFlipper>().enabled = false;
                }
            }
        }

        //Closes the Pause Menu so the player can continue playing
        public void ExitPauseMenu()
        {
            //deactivates the PauseMenu gameObject
            pauseMenu.SetActive(false);
            //makes the player move again
            player.GetComponent<Player>().enabled = true;
            foxSprite.GetComponent<SpriteFlipper>().enabled = true;
        }
    }
}

