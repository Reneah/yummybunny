﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class Player : MonoBehaviour
    {
        //declares the speed of the player with integer
        [SerializeField]
        private int moveSpeed;

        //declares how high the player can jump with integer
        [SerializeField]
        private int jumpHeight;

        //shows if the player is on the ground
        [SerializeField]
        private bool isGrounded;

        //references the Animator that is attached to the FoxSpite gameObject
        private Animator myAnimator;

        //initializesstring variable with BiteAnim to trigger the Animation with this name
        private string biteAnim = "BiteAnim";

        //references the rigidbody of the player
        private Rigidbody2D myRigidbody2D;

        //references the WinScreen gameObject
        [SerializeField]
        private GameObject winScreen;

        //references the FoxSprite gameObject
        [SerializeField]
        private GameObject foxSprite;

        //initializes string variable to "Goal"
        private string goalTag = "Goal";

        //creates an array that refrences different Audio Clips of bite sounds
        [SerializeField]
        private AudioClip[] biteClips;

        //references the Audio Source for the bite clips
        [SerializeField]
        private AudioSource biteAudioSource;

        //references the GroundLayer Layer
        [SerializeField]
        private LayerMask groundLayer;

        //initializes string variable to "PowerUp"
        private string powerUpTag = "PowerUp";


        private void Start()
        {
            //gets the Rigidbody2D component of the player gameObject this script is attached to
            myRigidbody2D = GetComponent<Rigidbody2D>();
            //gets the Animator component of the FoxSprite gameObject that is a child of the player gameObject
            myAnimator = GetComponentInChildren<Animator>();
        }

        //executes the MovePlayer() function with FixedUpdate because physics are used, prevents lagging
        private void FixedUpdate()
        {
             MovePlayer();
        }

        private void Update()
        { 
            //checks if the boolean isGrounded is true   
            if (isGrounded)
            {
                //if boolean is true and space is pressed, adds force to the player to make it jump
                if (Input.GetKeyDown(KeyCode.Space))
                {   
                    myRigidbody2D.AddForce(new Vector2(0, jumpHeight), ForceMode2D.Impulse);
                }   
            }
            //executes the Bite function
            Bite();
        }

        //gets and sets the value of the moveSpeed variable so the public MoveSpeed property can be used in other scripts
        public int MoveSpeed
        {
            get { return moveSpeed; }
            set { moveSpeed = value; }
        }

        //moves the player to left and right
        public void MovePlayer()
        {
            //checks input and moves player
            transform.Translate(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f);
        }

        //executes the CheckIfGrounded function when player is colliding with any collider
        void OnCollisionStay2D(Collision2D collider)
        {
            CheckIfGrounded();
        }

        //sets the isGrounded boolean on CollisionExit to false so the player can't jump endlessly
        void OnCollisionExit2D(Collision2D collider)
        {
            isGrounded = false;
        }

        //raycasts down 1f from the player position to check for collider with the GroundLayer and sets isGrounded boolean to true so the player can jump
        private void CheckIfGrounded()
        {
            Vector2 positionToCheck = transform.position;
            Vector2 direction = Vector2.down;
            float distance = 1f;

            RaycastHit2D hit = Physics2D.Raycast(positionToCheck, direction, distance, groundLayer);
            if (hit.collider != null)
            {
                isGrounded = true;
            }
        }

        //starts the Bite Attack
        void Bite()
        {
            //checks if E key is pressed
            if (Input.GetKeyDown(KeyCode.E))
            {
                //plays the referenced biteAnim Animation
                myAnimator.Play(biteAnim);
                //randomizes the bite sound that should be played from the array
                biteAudioSource.clip = biteClips[Random.Range(0, biteClips.Length)];
                //plays the sound
                biteAudioSource.Play();
                Debug.Log("Bite!");
            }
        }

        //checks for collider when player is entering one
        private void OnTriggerEnter2D(Collider2D otherCollider)
        {
            //checks if the gameObject the play collides with is tagged with "Goal"
            if (otherCollider.gameObject.tag == goalTag)
            {      
                //sets winScreen to active
                winScreen.SetActive(true);
                //stops the Player from moving
                GetComponent<Player>().enabled = false;
                foxSprite.GetComponent<SpriteFlipper>().enabled = false;
            }
            //checks if the gameObject the play collides with is tagged with "PowerUp"
            if (otherCollider.gameObject.tag == powerUpTag)
            {
                //executes the ChoosePowerUps function of the PowerUp that the player collides with
                otherCollider.gameObject.GetComponent<PowerUps>().ChoosePowerUp();
                //plays the AudioSource of the PowerUp gameObject that the player collides with
                otherCollider.gameObject.GetComponent<AudioSource>().Play();
                //disables the SpriteRenderer and BoxCollider of the PowerUp the player collides with
                otherCollider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                otherCollider.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }
}
