﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class DeleteScore : MonoBehaviour
    {
        //deletes all PlayerPref keys and values
        public void ResetScore()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Delete Highscore");
        }
    }
}

