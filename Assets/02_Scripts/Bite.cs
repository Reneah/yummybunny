﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class Bite : MonoBehaviour
    {
        //initializes string variable to "Bunny", meaning all gameObjects that are normal bunnies
        private string bunnyTag = "Bunny";
        //initializes this string variable to "Biteable", meaning all gameObjects that can only be bitten with the BitePowerUp
        private string biteableTag = "Biteable";

        //references the BunnyScorer Script in the BunnyScore gameObject
        [SerializeField]
        private BunnyScorer myBunnyScorer;

        //references the PowerUp that enables the bitePower
        [SerializeField]
        private PowerUps bitePowerUp;

        //specifies what happens when the player collides with collider of a bunny gameObject
        private void OnTriggerStay2D(Collider2D bunnyCollider)
        {
            //checks if the player presses E when he's colliding with a gameObject
            if (Input.GetKey(KeyCode.E))
            {
                //checks if the collider the player collides with is tagged with "Bunny"
                if (bunnyCollider.gameObject.tag == bunnyTag)
                {
                    //sets [bunnyScore] + 1 (in [BunnysScorer] Class)
                    myBunnyScorer.bunnyScore += 1;
                    //starts the Blood Splatter Particle Effect
                    BloodSplatter splatter = bunnyCollider.GetComponent<BloodSplatter>(); 
                    splatter.StartBloodSplatter();
                    //disables components from bunny so it is not visible anymore
                    bunnyCollider.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    bunnyCollider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                }

                //checks if the collider the player collides with is tagged with "Biteable"
                if (bunnyCollider.gameObject.tag == biteableTag)
                {
                    //plays the Audio Source that is attached to the collided gameObject
                    bunnyCollider.gameObject.GetComponent<AudioSource>().Play();
                    //checks if the player has the Bite Power and if the bool is active
                    if (bitePowerUp.PowerUpState == PowerUps.PowerUp.BitePower && bitePowerUp.CollectedBitePower)
                    {
                        //sets [bunnyScore] + 1 (in [BunnysScorer] Class)
                        myBunnyScorer.bunnyScore += 1;
                        bunnyCollider.gameObject.GetComponent<AudioSource>().Play();
                        //disables components from bunny so it is not visible anymore
                        bunnyCollider.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                        bunnyCollider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                    }
                }
            }
        }
    }
}
