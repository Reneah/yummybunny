﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class CameraMovement : MonoBehaviour
    {
        //references the transform component of the player gameObject
        [SerializeField]
        private Transform Player;

        //initializes an offset for the camera with float
        private float cameraOffset = 3f;

        //declares the x position of the camera with float
        private float cameraPositionX;

        void Update()
        {
            //sets the cameraPositionX variable to the player position with an offset
            cameraPositionX = Player.position.x + cameraOffset;
            //sets the x position of the camera to the cameraPositionX variable so the camera follows the player, y and z position stay fixed to the cameras original position
            transform.position = new Vector3(cameraPositionX, transform.position.y, transform.position.z);
        }
    }
}

