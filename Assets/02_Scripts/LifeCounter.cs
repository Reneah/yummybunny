﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YummyBunny
{
    public class LifeCounter : MonoBehaviour
    {
        //variable for Life 
        private int lifeValue;

        //references the first heart in the canvas
        [SerializeField]
        private GameObject herzOne;

        //references the second heart in the canvas
        [SerializeField]
        private GameObject herzTwo;

        //references the third heart in the canvas
        [SerializeField]
        private GameObject herzThree;

        //references the gameObject Pillow in the Canvas
        [SerializeField]
        private GameObject pillow;

        //declares a variable that checks if the player is dead with bool
        [SerializeField]
        private bool playerIsDead;

        //references the GameOverScreen gameObject
        [SerializeField]
        private GameObject gameOverScreen;

        //references the Player gameObject
        [SerializeField]
        private GameObject player;

        //references the FoxSpriteParent gameObject
        [SerializeField]
        private GameObject foxSpriteParent;

        //gets and sets the value of the lifeValue variable so the public LifeValue property can be used in other scripts
        public int LifeValue
        {
            get { return lifeValue; }
            set { lifeValue = value; }
        }

        // Start is called before the first frame update
        void Start()
        {
            //at the Game Start character has 3 Lifes - sets [lifeValue] to this number
            lifeValue = 3;
        }

        // Update is called once per frame
        void Update()
        {
            //makes sprites turn on and off by different values of lifeValue

            //checks if the player has 0 lifes and sets all red hearts inactive
            if (lifeValue <= 0)
            {
                herzOne.SetActive(false);
                herzTwo.SetActive(false);
                herzThree.SetActive(false);
                playerIsDead = true;
                Debug.Log("You are Dead");
            }

            //checks if the player has 1 life and sets one red heart active and two inactive
            if (lifeValue == 1)
            {
                herzOne.SetActive(false);
                herzTwo.SetActive(false);
                herzThree.SetActive(true);
            }

            //checks if the player has 2 lifes and sets two red hearts active and one inactive
            if (lifeValue == 2)
            {
                herzOne.SetActive(false);
                herzTwo.SetActive(true);
                herzThree.SetActive(true);

            }
        
            //checks if the player has 3 or more lifes and sets all red hearts active
            if(lifeValue >= 3)
            {
                herzOne.SetActive(true);
                herzTwo.SetActive(true);
                herzThree.SetActive(true);
                Debug.Log("3 Lifes");
            }

            //checks if the lifeValue is set to 4 and activates the referenced Pillow gameObject
            if (lifeValue == 4)
            {
                pillow.SetActive(true);
            }

            //checks if the bool [playerIsDead] is true and activates the Game Over screen
            if (playerIsDead)
            {
                gameOverScreen.SetActive(true);
                //disables Player and SpriteFlipper Script so the player can't move anymore
                player.GetComponent<Player>().enabled = false;
                foxSpriteParent.GetComponent<SpriteFlipper>().enabled = false;
            }
        }
    }

}
